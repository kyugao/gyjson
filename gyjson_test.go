package gyjson

import (
	"testing"
	"os"
	"fmt"
)

var Config struct {
	LogPath       string
	LogFile       string
	EnableConsole bool
	Level         string
}

func TestToMap(t *testing.T) {
	Config.LogFile = "test.log"
	Config.LogPath = "./"
	Config.EnableConsole = true
	Config.Level = "DEBUG"
	result := ToMap(Config)
	t.Logf(result)
}

func TestToJsonString(t *testing.T) {
	Config.LogFile = "test.log"
	Config.LogPath = "./"
	Config.EnableConsole = true
	Config.Level = "DEBUG"
	t.Log(ToJsonString(Config))
}

func TestToFile(t *testing.T) {

	Config.LogFile = "test.log"
	Config.LogPath = "./"
	Config.EnableConsole = true
	Config.Level = "DEBUG"

	file, err := os.Create("./conf/tofile.config")
	defer file.Close()
	if err != nil {
		fmt.Println("open file failed.", err.Error())
		return
	}
	ToFile(file, &Config)
}

func TestLoadConfig(t *testing.T) {

	Config.LogFile = "test.log"
	Config.LogPath = "./"
	Config.EnableConsole = true
	Config.Level = "DEBUG"

	FromFile("./conf/test.config", &Config)
	t.Log("Config = %s.\n", ToJsonString(&Config))
}