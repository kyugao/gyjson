package gyjson

import (
	"encoding/json"
	"io/ioutil"
	"fmt"
	"os"
)

func ToJsonString(any interface{}) (result string) {
	if any == nil {
		result = ""
	} else {
		bytes, err := json.Marshal(any)
		if err == nil {
			result = string(bytes)
		} else {
			result = ""
		}
	}
	return
}

func ToJsonByte(any interface{}) (result []byte) {
	if any == nil {
	} else {
		var err error
		result, err = json.Marshal(any)
		if err != nil {
			result = make([]byte, 0)
		}
	}
	return
}

func ToMap(any interface{}) (result map[string]interface{}) {
	bytes := ToJsonByte(any)
	if bytes == nil {
		result = nil
		return
	}

	if err := json.Unmarshal(bytes, &result); err != nil {
		return nil
	}
	return result
}

func FromBytes(input []byte, output interface{}) (err error) {
	err = json.Unmarshal(input, output)
	if err != nil {
		fmt.Errorf("Unmarshal error %v.\n", err)
	}
	return
}

func FromString(input string, output interface{}) (err error) {
	err = json.Unmarshal([]byte(input), output)
	if err != nil {
		fmt.Errorf("Unmarshal from string error %v.\n", err)
	}
	return
}

func FromFile(path string, config interface{}) (err error) {
	data, err := ioutil.ReadFile(path)
	if err != nil {
		fmt.Errorf("Read file error %v.\n", err)
	}
	err = json.Unmarshal(data, config)
	if err != nil {
		fmt.Errorf("Unmarshal error %v.\n", err)
	}
	return
}

func ToFile(file *os.File, config interface{}) (err error) {
	result := ToJsonString(config)
	fmt.Println("to file " + result)

	_, err = file.WriteString(result)
	fmt.Println(ToJsonString(err))
	return
}
